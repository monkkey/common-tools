<?php

namespace Monkkey\Tools\Hydrator;

use Monkkey\Tools\Hydrator\Exception\ObjectHydratorException;

/**
 * A simple generic object Hydrator.
 */
final class ObjectHydrator
{
    /**
     * Hydrate an object from an associative array.
     *
     * @param  object $instance    The instance to hydrate
     * @param  array  $objectArray The associative array to hydrate from
     * @return object
     */
    public function hydrate(object $instance, array $objectArray)
    {
        /** @var string[] $properties */
        $properties = array_keys($objectArray);

        // The hydrated instance
        return array_reduce(
            $properties,
            $this->toHydratedObject($objectArray),
            $instance
        );
    }

    /**
     * @param  array $objectArray
     * @return \Closure
     */
    private function toHydratedObject(array $objectArray)
    {
        /*
         * Set a value to the object property from the matching objectArray value.
         * Returns the updated object.
         *
         * @param  object $objectToHydrate
         * @param  string $property
         * @return object
         */
        return function (object $objectToHydrate, string $property) use ($objectArray) {
            try {
                $reflectionProperty = new \ReflectionProperty($objectToHydrate, $property);
                $reflectionProperty->setAccessible(true);
                $reflectionProperty->setValue($objectToHydrate, $objectArray[$property]);
            } catch (\ReflectionException $e) {
                // (^_^)
            }

            return $objectToHydrate;
        };
    }
}
