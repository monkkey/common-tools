<?php

namespace Monkkey\Tools\Finder;

interface ObjectFinderInterface
{
    /**
     * Create a Needle from an object
     *
     * @param  object $object
     * @return NeedleInterface
     */
    public function search(object $object): NeedleInterface;
}
