<?php

namespace Monkkey\Tools\Finder;

final class Finder
{
    /**
     * @var IndexFinder
     */
    private $indexFinder;

    /**
     * @var ItemFinder
     */
    private $itemFinder;

    public function __construct()
    {
        $this->indexFinder = new IndexFinder();
        $this->itemFinder  = new ItemFinder();
    }
    /**
     * Returns the index of the matching item.
     * If there is no match, -1 is returned
     *
     * @param array    $collection The array of items to search in.
     * @param callable $callback   Called on every item of the collection. Must return a boolean.
     * @return int                 The index of the matching item, -1 if there is not match
     */
    public function findFirstIndex(array $collection, callable $callback): int
    {
        return $this->indexFinder->findFirst($collection, $callback);
    }

    /**
     * Returns the indexes of the matching items based on a callback.
     *
     * @param  array    $collection The array of items to search in.
     * @param  callable $callback   Called on every item of the collection. Must return a boolean.
     * @return array                The indexes of the matching items, an emtpy array if there is not match.
     */
    public function findManyIndexes(array $collection, callable $callback)
    {
        return $this->indexFinder->findMany($collection, $callback);
    }

    /**
     * Returns the first match in the collection based on a callback.
     * If there is no match, null is returned.
     *
     * @param array    $collection The array of items to search in.
     * @param callable $callback   Called on every item of the collection. Must return a boolean.
     * @return mixed               The first match or null if there is not match.
     */
    public function findFirstItem(array $collection, callable $callback)
    {
        return $this->itemFinder->findFirst($collection, $callback);
    }

    /**
     * Returns the matches in the collection based on a callback.
     * If there is no match, an empty array is returned.
     *
     * @param  array    $collection The array of items to search in.
     * @param  callable $callback   Called on every item of the collection. Must return a boolean.
     * @return array                The matches array or an empty array if there is not match.
     */
    public function findManyItems(array $collection, callable $callback)
    {
        return $this->itemFinder->findMany($collection, $callback);
    }
}
