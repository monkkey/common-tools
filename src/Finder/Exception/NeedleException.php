<?php

namespace Monkkey\Tools\Finder\Exception;

final class NeedleException extends \LogicException
{
}
