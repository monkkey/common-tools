<?php

namespace Monkkey\Tools\Finder;

final class IndexFinder
{
    /**
     * Returns the index of the matching item.
     * If there is no match, -1 is returned
     *
     * @param array    $collection The array of items to search in.
     * @param callable $callback   Called on every item of the collection. Must return a boolean.
     * @return int                 The index of the matching item, -1 if there is not match
     */
    public function findFirst(array $collection, callable $callback): int
    {
        $keys = array_keys($collection);

        $reduceToMatchingIndex = function (array $collection, callable $callback): \Closure {
            /**
             * @param  int|string $match
             * @param  int|string $key
             * @return int|string
             */
            return function ($match, $key) use ($collection, $callback) {
                // The needle has been found
                if (-1 !== $match) {
                    return $match;
                }
                // The needle matches
                if ($callback($collection[$key])) {
                    return $key;
                }
                return -1;
            };
        };

        return array_reduce(
            $keys,
            $reduceToMatchingIndex($collection, $callback),
            null
        );
    }

    /**
     * Returns the indexes of the matching items based on a callback.
     *
     * @param  array    $collection The array of items to search in.
     * @param  callable $callback   Called on every item of the collection. Must return a boolean.
     * @return array                The indexes of the matching items, an emtpy array if there is not match.
     */
    public function findMany(array $collection, callable $callback)
    {
        $keys = array_keys($collection);

        $reduceToMatchingIndexes = function (array $collection, callable $callback): \Closure {
            /**
             * @param  int[]|string[] $matches
             * @param  int|string     $key
             * @return int[]|string[]
             */
            return function ($matches, $key) use ($collection, $callback) {
                // The needle matches
                if ($callback($collection[$key])) {
                    $matches[] = $key;
                }
                return $matches;
            };
        };

        return array_reduce(
            $keys,
            $reduceToMatchingIndexes($collection, $callback),
            []
        );
    }
}
