<?php

namespace Monkkey\Tools\Sorter;

use Monkkey\Tools\Sorter\Exception\ObjectSorterException;

final class ObjectSorter implements SorterInterface
{
    const ASC  = 1;
    const DESC = -1;

    /**
     * {@inheritDoc}
     */
    public function sortBy(
        array $objectsToSort,
        string $property,
        int $direction = self::ASC
    ) {
        if (count($objectsToSort) <= 1) {
            return $objectsToSort;
        }

        $sortedObjects = array_slice($objectsToSort, 0);

        usort($sortedObjects, function ($current, $next) use ($property, $direction) {

            if (!property_exists($current, $property) || !property_exists($next, $property)) {
                $message = "The provided array of objects cannot be sorted by {$property}.";
                $message .= " At least one of the objects is missing the property";
                throw new ObjectSorterException($message);
            }

            $currentProperty = new \ReflectionProperty($current, $property);
            $currentProperty->setAccessible(true);
            $currentValue = $currentProperty->getValue($current);

            $nextProperty = new \ReflectionProperty($next, $property);
            $nextProperty->setAccessible(true);
            $nextValue = $nextProperty->getValue($next);

            return $direction * ($currentValue < $nextValue ? -1 : 1);
        });

        return $sortedObjects;
    }
}
