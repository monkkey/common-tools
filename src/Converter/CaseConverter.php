<?php

namespace Monkkey\Tools\Converter;

final class CaseConverter
{
    /**
     * Converts a snake case string into camel case
     *
     * @param  string $input
     * @return string
     */
    public function snakeToCamel(string $input): string
    {
        return lcfirst($this->snakeToPascal($input));
    }

    /**
     * Converts a snake case string into camel case
     *
     * @param  string $input
     * @return string
     */
    public function snakeToPascal(string $input): string
    {
        return str_replace("_", "", ucwords($input, "_"));
    }

    /**
     * Converts a camel case string into snake case
     *
     * @param  string $input
     * @return string
     */
    public function camelToSnake(string $input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
    }

    /**
     * Returns an array with the same values and the keys transformed by a converter
     *
     * @param array    $data
     * @param callable $converter
     * @return array
     */
    public function convertIndexes(array $data, callable $converter)
    {
        $keys = array_keys($data);

        return array_reduce($keys, function (array $convertedArray, string $field) use ($data, $converter) {
            $convertedArray[$converter($field)] = $data[$field];
            return $convertedArray;
        }, []);
    }
}
