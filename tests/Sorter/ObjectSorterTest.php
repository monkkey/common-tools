<?php

namespace Monkkey\Tests\Sorter;

use PHPUnit\Framework\TestCase;
use Monkkey\Tests\CanCreateInstanceTrait;
use Monkkey\Tools\Sorter\ObjectSorter;

final class ObjectSorterTest extends TestCase
{
    use CanCreateInstanceTrait;

    /**
     * @test
     */
    public function sortBySuccessful()
    {
        $sorter = new ObjectSorter();
        $objectsToSort = $this->getObjectsToSort();
        $sortedObjects = $sorter->sortBy($objectsToSort, "id");

        $this->assertTrue(is_array($sortedObjects), "The sortBy function should return an array");

        for ($i = 0; $i < count($sortedObjects) - 1; $i++) {
            $currentId = $this->getPropertyValue($sortedObjects[$i], "id");
            $nextId = $this->getPropertyValue($sortedObjects[$i + 1], "id");

            $this->assertTrue($currentId < $nextId, "The objects should be sorted by ascending id");
        };
    }

    public function getPropertyValue(object $instance, string $property)
    {
        $instanceProperty = new \ReflectionProperty($instance, $property);
        $instanceProperty->setAccessible(true);

        return $instanceProperty->getValue($instance);
    }

    private function getObjectsToSort()
    {
        return [
            $this->getHydratedInstance([
                "id"   => 2,
                "name" => "Lëon",
            ]),
            $this->getHydratedInstance([
                "id"   => 1,
                "name" => "Maverick",
            ]),
            $this->getHydratedInstance([
                "id"   => 3,
                "name" => "Denzel",
            ]),
        ];
    }
}
