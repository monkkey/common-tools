<?php

declare(strict_types=1);

namespace Monkkey\Tests\Finder;

use PHPUnit\Framework\TestCase;
use Monkkey\Tests\CanCreateInstanceTrait;
use Monkkey\Tools\Finder\ItemFinder;

final class ItemFinderTest extends TestCase
{
    use CanCreateInstanceTrait;

    /**
     * @var ItemFinder
     */
    private static $finder = null;

    /**
     * @test
     */
    public function findAll()
    {
        $finder = $this->getFinder();
        $collection = $this->getTestCollection();

        $allItems = $finder->findMany($collection, function ($item) {
            return true;
        });
        
        $countMessage = "Searching for items with a callback returning true should return an array of the same size";
        $this->assertCount(count($collection), $allItems, $countMessage);
    }

    /**
     * @test
     */
    public function findWithoutMatch()
    {
        $finder = $this->getFinder();
        $objectsToSearchIn = $this->getTestCollection();

        $noMatchItems = $finder->findMany($objectsToSearchIn, function ($object) {
            return false;
        });

        $countMessage = "Searching for items with a callback returning false should return an empty array";
        $this->assertCount(0, $noMatchItems, $countMessage);
    }

    /**
     * @test
     */
    public function findMany()
    {
        $finder = $this->getFinder();
        $objectsToSearchIn = $this->getTestCollection();

        $twoFirstIndexes = $finder->findMany($objectsToSearchIn, function ($object) {
            return $object->getId() < 3;
        });

        $this->assertCount(2, $twoFirstIndexes);
        $this->assertSame(1, $twoFirstIndexes[0]->getId(), "The 1st value should be Jon (id = 1)");
        $this->assertSame(2, $twoFirstIndexes[1]->getId(), "The 2nd value should be Alfonse (id = 2)");

        $indexesOfOddIds = $finder->findMany($objectsToSearchIn, function ($object) {
            return $object->getId() % 2 !== 0;
        });

        $this->assertCount(2, $indexesOfOddIds);
        $this->assertSame(1, $indexesOfOddIds[0]->getId(), "The 1st value should be Jon (id = 1)");
        $this->assertSame(3, $indexesOfOddIds[1]->getId(), "The 2nd value should be Emmet (id = 3)");
    }

    /**
     * @return array
     */
    public function getTestCollection()
    {
        return [
            $this->getHydratedInstance([
                "id"   => 1,
                "name" => "Jon",
            ]),
            $this->getHydratedInstance([
                "id"   => 2,
                "name" => "Alfonse",
            ]),
            $this->getHydratedInstance([
                "id"   => 3,
                "name" => "Emmet",
            ]),
            $this->getHydratedInstance([
                "id"   => 4,
                "name" => "Clarence",
            ]),
        ];
    }

    private function getFinder(): ItemFinder
    {
        if (null === static::$finder) {
            static::$finder = new ItemFinder();
        }

        return static::$finder;
    }
}
