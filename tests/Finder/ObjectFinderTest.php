<?php

declare(strict_types=1);

namespace Monkkey\Tests\Finder;

use PHPUnit\Framework\TestCase;
use Monkkey\Tools\Hydrator\ObjectHydrator;
use Monkkey\Tools\Finder\ObjectFinder;
use Monkkey\Tests\CanCreateInstanceTrait;

final class ObjectFinderTest extends TestCase
{
    use CanCreateInstanceTrait;

    private $publicProperty  = "name";
    private $privateProperty = "id";
    
    /**
     * @test
     * @dataProvider existingObject
     */
    public function searchExistingObjectShouldSucceed($objectToFind)
    {
        $objectsToSearchIn = $this->getFakeData();
        
        $finder = new ObjectFinder();

        $objectFoundByPublicProperty = $finder
            ->search($objectToFind)
            ->by($this->publicProperty)
            ->in($objectsToSearchIn);

        $failureMessage = "Existing object SHOULD be found (public property) {$this->publicProperty}";
        $this->assertTrue($objectFoundByPublicProperty, $failureMessage);
        
        $objectFoundByPrivateProperty = $finder
            ->search($objectToFind)
            ->by($this->privateProperty)
            ->in($objectsToSearchIn);
        
        $failureMessage = "Existing object SHOULD be found (private property) {$this->privateProperty}";
        $this->assertTrue($objectFoundByPrivateProperty, $failureMessage);
    }

    /**
     * @test
     * @dataProvider nonExistingObject
     */
    public function searchNonExistingObjectShouldFail($objectToFind)
    {
        $objectsToSearchIn = $this->getFakeData();
        
        $finder = new ObjectFinder();

        $objectHasBeenFound = $finder
            ->search($objectToFind)
            ->by($this->publicProperty)
            ->in($objectsToSearchIn);

        $objectToFindProperty = new \ReflectionProperty($objectToFind, "id");
        $objectToFindProperty->setAccessible(true);
        $objectToFindId = $objectToFindProperty->getValue($objectToFind);

        $failureMessage = "Non-existing object SHOULD NOT be found (by public property) {$this->publicProperty}";
        $this->assertFalse($objectHasBeenFound, $failureMessage);
    }

    /**
     * @return array
     */
    public function existingObject()
    {
        return [
            [
                $this->getHydratedInstance([
                    "id"   => 1,
                    "name" => "Jon",
                ]),
            ],
            [
                $this->getHydratedInstance([
                    "id"   => 2,
                    "name" => "Alfonse",
                ]),
            ],
        ];
    }

    /**
     * @return array
     */
    public function nonExistingObject()
    {
        return [
            [
                $this->getHydratedInstance([
                    "id"   => 3,
                    "name" => "ra@punzel.io",
                ]),
            ],
            [
                $this->getHydratedInstance([
                    "id"   => 4,
                    "name" => "me@rida.io",
                ]),
            ],
        ];
    }

    /**
     * @return array
     */
    public function getFakeData()
    {
        return [
            $this->getHydratedInstance([
                "id"   => 1,
                "name" => "Jon",
            ]),
            $this->getHydratedInstance([
                "id"   => 2,
                "name" => "Alfonse",
            ]),
            $this->getHydratedInstance([
                "id"   => 3,
                "name" => "Emmet",
            ]),
        ];
    }
}
